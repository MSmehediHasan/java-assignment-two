import java.util.Scanner;


public class EmployeeSalary {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int sum = 0;
		int comAcc2 = 0;
		System.out.println("Enter Basic Salary of Lowest grade: ");
		int basicSalary = in.nextInt();
		System.out.println("Enter Balance of Company Bank Account: ");
		int comAcc = in.nextInt();
		int comAcc1 = comAcc;
		//											 "Name", "Grade", "Address", "Mobile", "Bank Acc_no"
		String EmployeeInfo[][] = new String[][] { { "Rahim", "6", "Agrabad", "01800000000", "01" },
													{ "Karim", "6", "GEC", "01811111111", "02" },
													{ "Rahat", "5", "ChowkBazar", "01900000000", "03" },
													{ "Mahadi", "5", "New Market", "01700000000", "04" },
													{ "Fahad", "4", "Agrabad", "01600000000", "05" },
													{ "Fahim", "4", "Bohoddarhat", "01911111111", "06" },
													{ "Fahima", "3", "Muradpur", "01610101010", "07" },
													{ "Sayem", "3", "AK Khan", "01710101010", "08" },
													{ "Jamil", "2", "GEC", "01510101010", "09" },
													{ "Reza", "1", "Kotwali", "01511111111", "10" } }; 
								//"Current/Saving" "Acc_Name","Acc_no", "Balance", "Branch", "Bank"	
		String BankInfo[][] = new String[][] { { "C", "Rahim", "01", "1000", "Agrabad", "EBL" },
												{ "C", "Karim", "02", "5000", "GEC", "EBL" },
												{ "C", "Rahat", "03", "2000", "ChowkBazar", "EBL" },
												{ "C", "Mahadi", "04", "1200", "New Market", "EBL" },
												{ "C", "Fahad", "05", "900", "Agrabad", "EBL" },
												{ "C", "Fahim", "06", "0", "Bohoddarhat", "EBL" },
												{ "C", "Fahima", "07", "500", "Muradpur", "EBL" },
												{ "C", "Sayem", "08", "1500", "AK Khan", "EBL" },
												{ "C", "Jamil", "09", "220", "GEC", "EBL" },
												{ "C", "Reza", "10", "1250", "Kotwali", "EBL" } }; 
		int[][] GradeSalary = new int[7][5];
		//Generate Grade wise salary 
		for (int i = 6; i >= 1; i--) {
			if (i == 6) {
				GradeSalary[i][1] = basicSalary;
				GradeSalary[i][2] = (int) (GradeSalary[i][1] * 0.15);
				GradeSalary[i][3] = (int) (GradeSalary[i][1] * 0.20);
				GradeSalary[i][4] = GradeSalary[i][1] + GradeSalary[i][2] + GradeSalary[i][3];
			} else {
				GradeSalary[i][1] = GradeSalary[i + 1][1] + 5000;
				GradeSalary[i][2] = (int) (GradeSalary[i][1] * 0.15);
				GradeSalary[i][3] = (int) (GradeSalary[i][1] * 0.20);
				GradeSalary[i][4] = GradeSalary[i][1] + GradeSalary[i][2] + GradeSalary[i][3];
			}
		}
		int i = 0;
		while (i < 10) {
			if (EmployeeInfo[i][1] == "1") {

				if (comAcc >= GradeSalary[1][4]) {
					int temp = Integer.parseInt(BankInfo[i][3]);
					int temp1 = temp + GradeSalary[1][4];
					sum = sum + GradeSalary[1][4];
					BankInfo[i][3] = Integer.toString(temp1);
					System.out.println("Name: " + EmployeeInfo[i][0] + " | Grade: " + EmployeeInfo[i][1]
							+ " | Previous Balance: " + temp + " | Salary: " + GradeSalary[1][4]
							+ " | Current Balance: " + BankInfo[i][3]);
					comAcc = comAcc - GradeSalary[1][4];
					i++;
				} else {
					System.out.println("Enter More Balance in Company Bank Account: ");
					comAcc2 = in.nextInt();
					comAcc = comAcc + comAcc2;
				}

			} else if (EmployeeInfo[i][1] == "2") {
				if (comAcc >= GradeSalary[2][4]) {
					int temp = Integer.parseInt(BankInfo[i][3]);
					int temp1 = temp + GradeSalary[2][4];
					sum = sum + GradeSalary[2][4];
					BankInfo[i][3] = Integer.toString(temp1);
					System.out.println("Name: " + EmployeeInfo[i][0] + " | Grade: " + EmployeeInfo[i][1]
							+ " | Previous Balance: " + temp + " | Salary: " + GradeSalary[2][4]
							+ " | Current Balance: " + BankInfo[i][3]);
					comAcc = comAcc - GradeSalary[2][4];
					i++;
				} else {
					System.out.println("Enter More Balance in Company Bank Account: ");
					comAcc2 = in.nextInt();
					comAcc = comAcc + comAcc2;
				}
			} else if (EmployeeInfo[i][1] == "3") {
				if (comAcc >= GradeSalary[3][4]) {
					int temp = Integer.parseInt(BankInfo[i][3]);
					int temp1 = temp + GradeSalary[3][4];
					sum = sum + GradeSalary[3][4];
					BankInfo[i][3] = Integer.toString(temp1);
					System.out.println("Name: " + EmployeeInfo[i][0] + " | Grade: " + EmployeeInfo[i][1]
							+ " | Previous Balance: " + temp + " | Salary: " + GradeSalary[3][4]
							+ " | Current Balance: " + BankInfo[i][3]);
					comAcc = comAcc - GradeSalary[3][4];
					i++;
				} else {
					System.out.println("Enter More Balance in Company Bank Account: ");
					comAcc2 = in.nextInt();
					comAcc = comAcc + comAcc2;
				}
			} else if (EmployeeInfo[i][1] == "4") {
				if (comAcc >= GradeSalary[4][4]) {
					int temp = Integer.parseInt(BankInfo[i][3]);
					int temp1 = temp + GradeSalary[4][4];
					sum = sum + GradeSalary[4][4];
					BankInfo[i][3] = Integer.toString(temp1);
					System.out.println("Name: " + EmployeeInfo[i][0] + " | Grade: " + EmployeeInfo[i][1]
							+ " | Previous Balance: " + temp + " | Salary: " + GradeSalary[4][4]
							+ " | Current Balance: " + BankInfo[i][3]);
					comAcc = comAcc - GradeSalary[4][4];
					i++;
				} else {
					System.out.println("Enter More Balance in Company Bank Account: ");
					comAcc2 = in.nextInt();
					comAcc = comAcc + comAcc2;
				}
			} else if (EmployeeInfo[i][1] == "5") {
				if (comAcc >= GradeSalary[5][4]) {
					int temp = Integer.parseInt(BankInfo[i][3]);
					int temp1 = temp + GradeSalary[5][4];
					sum = sum + GradeSalary[5][4];
					BankInfo[i][3] = Integer.toString(temp1);
					System.out.println("Name: " + EmployeeInfo[i][0] + " | Grade: " + EmployeeInfo[i][1]
							+ " | Previous Balance: " + temp + " | Salary: " + GradeSalary[5][4]
							+ " | Current Balance: " + BankInfo[i][3]);
					comAcc = comAcc - GradeSalary[5][4];
					i++;
				} else {
					System.out.println("Enter More Balance in Company Bank Account: ");
					comAcc2 = in.nextInt();
					comAcc = comAcc + comAcc2;
				}
			} else if (EmployeeInfo[i][1] == "6") {
				if (comAcc >= GradeSalary[6][4]) {
					int temp = Integer.parseInt(BankInfo[i][3]);
					int temp1 = temp + GradeSalary[6][4];
					sum = sum + GradeSalary[6][4];
					BankInfo[i][3] = Integer.toString(temp1);
					System.out.println("Name: " + EmployeeInfo[i][0] + " | Grade: " + EmployeeInfo[i][1]
							+ " | Previous Balance: " + temp + " | Salary: " + GradeSalary[6][4]
							+ " | Current Balance: " + BankInfo[i][3]);
					comAcc = comAcc - GradeSalary[6][4];
					i++;
				} else {
					System.out.println("Enter More Balance in Company Bank Account: ");
					comAcc2 = in.nextInt();
					comAcc = comAcc + comAcc2;
				}
			}

		}
		System.out.println("Remaining Balance: " + comAcc);
		System.out.println("Total Paid Amount: " + sum);
		/*for (i = 1; i <= 6; i++) {
			System.out.println("Grade: " + i + " | Basic: " + GradeSalary[i][1] + " | House Rent: " + GradeSalary[i][2]
					+ " | Medical Allowance: " + GradeSalary[i][3] + " | Total: " + GradeSalary[i][4]);
		}*/
	}

}
