import java.util.Scanner;

public class DayName {
    public static void main(String[] args) {

        int dayNumber;
        String dayName;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter Day Number: ");
        dayNumber = scanner.nextInt();
        switch (dayNumber) {
            case 1:
                dayName = "Sunday";
                break;
            case 2:
                dayName = "Monday";
                break;
            case 3:
                dayName = "Tuesday";
                break;
            case 4:
                dayName = "Wednesday";
                break;
            case 5:
                dayName = "Thursday";
                break;
            case 6:
                dayName = "Friday";
                break;
            case 7:
                dayName = "Saturday";
            case 8:
                dayName = "Sunday";
                break;
            case 9:
                dayName = "Monday";
                break;
            case 10:
                dayName = "Tuesday";
                break;
            case 11:
                dayName = "Wednesday";
                break;
            case 12:
                dayName = "Thursday";
                break;
            case 13:
                dayName = "Friday";
                break;
            case 14:
                dayName = "Saturday";
                break;
            case 15:
                dayName = "Sunday";
                break;
            case 16:
                dayName = "Monday";
                break;
            case 17:
                dayName = "Tuesday";
                break;
            case 18:
                dayName = "Wednesday";
                break;
            case 19:
                dayName = "Thursday";
                break;
            case 20:
                dayName = "Friday";
                break;
            case 21:
                dayName = "Saturday";
            case 22:
                dayName = "Sunday";
                break;
            case 23:
                dayName = "Monday";
                break;
            case 24:
                dayName = "Tuesday";
                break;
            case 25:
                dayName = "Wednesday";
                break;
            case 26:
                dayName = "Thursday";
                break;
            case 27:
                dayName = "Friday";
                break;
            case 28:
                dayName = "Saturday";
                break;
            case 29:
                dayName = "Sunday";
                break;
            case 30:
                dayName = "Monday";
                break;
            case 31:
                dayName = "Tuesday";
                break;
            default:
                dayName = "Invalid day";
                break;
        }
        System.out.println("The day is " + dayName);
    }
}